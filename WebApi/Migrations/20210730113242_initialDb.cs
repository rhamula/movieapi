﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class initialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CharacterFullName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CharacterAlias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CharacterGender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CharacterPicture = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.CharacterId);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FranchiseName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    FranchiseDescription = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.FranchiseId);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    MovieGenre = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    MovieReleaseYear = table.Column<int>(type: "int", maxLength: 100, nullable: false),
                    MovieDirector = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    MoviePicture = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    MovieTrailer = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.MovieId);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "FranchiseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CharactersMovie",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharactersMovie", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_CharactersMovie_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "CharacterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharactersMovie_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "CharacterId", "CharacterAlias", "CharacterFullName", "CharacterGender", "CharacterPicture" },
                values: new object[,]
                {
                    { 1, "Katniss Everdeen", "Jennifer Lawrence", "Female", "https://www.imdb.com/title/tt1392170/mediaviewer/rm2148839936/?ft0=name&fv0=nm2225369&ft1=image_type&fv1=still_frame" },
                    { 2, "Harry Potter", "Sean Skinner", "StrongMale", "https://www.imdb.com/title/tt0241527/mediaviewer/rm785201408/" },
                    { 3, "Kiara", "Neve Campbell", "Female", "https://lionking.fandom.com/wiki/Kiara?file=Kiara-HD.png" },
                    { 4, "Peeta Mellark", "Josh Hutcherson", "Male", "https://www.imdb.com/title/tt1951265/mediaviewer/rm1779759872/?ft0=name&fv0=nm1242688&ft1=image_type&fv1=still_frame" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "FranchiseId", "FranchiseDescription", "FranchiseName" },
                values: new object[,]
                {
                    { 1, "The Hunger Games film series is composed of science fiction dystopian adventure films", "Hunger Games" },
                    { 2, "Harry Potter is a guy with glasses and he can do magic", "Harry Potter" },
                    { 3, "Lions living on prayer", "The Lion King" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieId", "FranchiseId", "MovieDirector", "MovieGenre", "MoviePicture", "MovieReleaseYear", "MovieTitle", "MovieTrailer" },
                values: new object[,]
                {
                    { 1, 1, "Francis Lawrence", "Adventure,Science Fiction", "https://en.wikipedia.org/wiki/The_Hunger_Games:_Mockingjay,_Part_1_(soundtrack)#/media/File:The_Hunger_Games_Mockingjay,_Part_1_%E2%80%93_Original_Motion_Picture_Soundtrack.png", 2014, "Mocking Jay Part 1", "https://www.youtube.com/watch?v=mg8vRvdZaFE" },
                    { 2, 2, "Chris Columbus", "Action", "https://harrypotter.fandom.com/wiki/Harry_Potter_and_the_Philosopher%27s_Stone_(film)?file=PS_poster.jpg", 2001, "Philosopher's stone", "https://www.youtube.com/watch?v=mNgwNXKBEW0" },
                    { 4, 2, "Chris Columbus", "Adventure", "https://www.imdb.com/title/tt0295297/mediaviewer/rm1029675264/", 2002, "Chamber of Secrets", "https://www.youtube.com/watch?v=jBltxS8HfQ4" },
                    { 3, 3, "Darrell Rooney", "Animation", "https://www.imdb.com/title/tt0120131/mediaviewer/rm1150299904/", 1998, "Simbas Pride", "https://www.youtube.com/watch?v=aylCi6thmHM" }
                });

            migrationBuilder.InsertData(
                table: "CharactersMovie",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 1 },
                    { 2, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharactersMovie_CharacterId",
                table: "CharactersMovie",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharactersMovie");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
