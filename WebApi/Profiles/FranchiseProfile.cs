﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MoviesApi.Models;
using WebApi.Models.DTOs.Character;
using WebApi.Models.DTOs.Franchise;
using WebApi.Models.DTOs.Movie;
using WebApi.Models.DTOs.MovieFranchise;

namespace WebApi.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m=>m.MovieId).ToList()))
                .ReverseMap();

            CreateMap<Franchise, FranchiseCreateDTO>()
                .ReverseMap();

            CreateMap<Franchise, FranchiseEditDTO>()
                .ReverseMap();

            CreateMap<Franchise, MovieFranchiseReadDTO>()
                .ForMember(mdto => mdto.Movies, opt => opt
                    .MapFrom(m => m.Movies.Select(x => x.MovieId).ToList())).ReverseMap();

            CreateMap<IEnumerable<Character>, CharacterFranchiseReadDTO>()
                .ForMember(cdto => cdto.Characters, opt => opt
                    .MapFrom(c => c.Select(c => c.CharacterId).ToList())).ReverseMap();

        }
        
        
    }
}
