﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MoviesApi.Models;
using WebApi.Models.DTOs.Character;

namespace WebApi.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                    .MapFrom(c => c.Movies.Select(m=>m.MovieId).ToList()))
                .ReverseMap();

            CreateMap<Character, CharacterEditDTO>()
                .ReverseMap();

            CreateMap<Character, CharacterCreateDTO>()
                .ReverseMap();
        }
    }
}
