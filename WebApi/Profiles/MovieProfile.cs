﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MoviesApi.Models;
using WebApi.Models.DTOs.CharacterMovie;
using WebApi.Models.DTOs.Movie;
using WebApi.Models.DTOs.MovieFranchise;

namespace WebApi.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                    .ForMember(mdto => mdto.Franchise, opt => opt
                    .MapFrom(m => m.FranchiseId))
                    .ForMember(mdto => mdto.Characters, opt => opt
                        .MapFrom(m => m.Characters.Select(c=>c.CharacterId).ToList()))
                    .ReverseMap();

            CreateMap<Movie, MovieEditDTO>()
                .ReverseMap();

            CreateMap<Movie, MovieCreateDTO>()
                .ReverseMap();

            CreateMap<Movie, MovieFranchiseEditDTO>()
                .ReverseMap();


            CreateMap<Movie,CharacterMovieReadDTO >()
                .ForMember(mdto => mdto.Characters, opt => opt
                    .MapFrom(m => m.Characters.Select(x => x.CharacterId).ToList())).ReverseMap();

        }
    }
}
