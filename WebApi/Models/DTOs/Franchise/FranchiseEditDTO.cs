﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models.DTOs.Franchise
{
    public class FranchiseEditDTO
    {
        public int FranchiseId { get; set; }
        public string FranchiseName { get; set; }
        public string FranchiseDescription { get; set; }
    }
}
