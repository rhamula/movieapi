﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models.DTOs.CharacterMovie
{
    public class CharacterMovieDeleteDTO
    {
        public int CharacterId { get; set; }
        public int MovieId { get; set; }
    }
}
