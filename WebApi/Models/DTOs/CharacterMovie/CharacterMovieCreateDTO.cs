﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace WebApi.Models.DTOs.CharacterMovie
{
    public class CharacterMovieCreateDTO
    {
        public int CharacterId { get; set; }
        public int MovieId { get; set; }
    }
}
