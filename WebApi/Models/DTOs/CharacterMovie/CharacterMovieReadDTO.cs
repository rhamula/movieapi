﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace WebApi.Models.DTOs.CharacterMovie
{
    public class CharacterMovieReadDTO
    {
        public List<int> Characters { get; set; }
    }
}
