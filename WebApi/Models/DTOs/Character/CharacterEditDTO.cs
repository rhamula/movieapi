﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models.DTOs.Character
{
    public class CharacterEditDTO
    {
        public int CharacterId { get; set; }

        public string CharacterFullName { get; set; }

        public string CharacterAlias { get; set; }

        public string CharacterGender { get; set; }

        public string CharacterPicture { get; set; }
    }
}
