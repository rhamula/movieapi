﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace WebApi.Models.DTOs.MovieFranchise
{
    public class MovieFranchiseReadDTO
    {
        public List<int> Movies { get; set; }

    }
}
