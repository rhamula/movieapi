﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models.DTOs.MovieFranchise
{
    public class MovieFranchiseEditDTO
    {
        public int MovieId { get; set; }
        public int FranchiseId { get; set; }

    }
}
