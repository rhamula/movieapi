﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models.DTOs.MovieFranchise
{
    public class CharacterFranchiseReadDTO
    {
        public List<int>  Characters { get; set; }

    }
}
