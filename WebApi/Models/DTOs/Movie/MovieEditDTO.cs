﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models.DTOs.Movie
{
    public class MovieEditDTO
    {
        public int MovieId { get; set; }
        public string MovieTitle { get; set; }
        public string MovieGenre { get; set; }
        public int MovieReleaseYear { get; set; }
        public string MovieDirector { get; set; }
        public string MoviePicture { get; set; }
        public string MovieTrailer { get; set; }
        public int FranchiseId { get; set; }
    }
}
