﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using WebApi.Models.DTOs.Franchise;

namespace MoviesApi.Models
{
    public class MoviesDbContext : DbContext
    {
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies {  get; set; }
        public DbSet<Character> Characters { get; set; }
        public MoviesDbContext([NotNull] DbContextOptions options) : base(options)
        {

        }

        /// <summary>
        /// Creates seed data
        /// </summary>
        /// <param name="modelBuilder">Model builder</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 1,FranchiseName = "Hunger Games",FranchiseDescription = "The Hunger Games film series is composed of science fiction dystopian adventure films" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 2, FranchiseName = "Harry Potter",FranchiseDescription = "Harry Potter is a guy with glasses and he can do magic"});
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 3, FranchiseName = "The Lion King", FranchiseDescription = "Lions living on prayer"});

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieId = 1, MovieTitle = "Mocking Jay Part 1", MovieGenre = "Adventure,Science Fiction",
                MovieReleaseYear = 2014, MovieDirector = "Francis Lawrence",
                MoviePicture =
                    "https://en.wikipedia.org/wiki/The_Hunger_Games:_Mockingjay,_Part_1_(soundtrack)#/media/File:The_Hunger_Games_Mockingjay,_Part_1_%E2%80%93_Original_Motion_Picture_Soundtrack.png",
                MovieTrailer = "https://www.youtube.com/watch?v=mg8vRvdZaFE",
                FranchiseId = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieId = 2, MovieTitle = "Philosopher's stone", MovieGenre = "Action", MovieReleaseYear = 2001,
                MovieDirector = "Chris Columbus",
                MoviePicture = "https://harrypotter.fandom.com/wiki/Harry_Potter_and_the_Philosopher%27s_Stone_(film)?file=PS_poster.jpg",
                MovieTrailer = "https://www.youtube.com/watch?v=mNgwNXKBEW0",
                FranchiseId = 2
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieId = 3, MovieTitle = "Simbas Pride", MovieGenre = "Animation", MovieReleaseYear = 1998,
                MovieDirector = "Darrell Rooney",
                MoviePicture = "https://www.imdb.com/title/tt0120131/mediaviewer/rm1150299904/",
                MovieTrailer = "https://www.youtube.com/watch?v=aylCi6thmHM",
                FranchiseId = 3
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieId = 4,
                MovieTitle = "Chamber of Secrets",
                MovieGenre = "Adventure",
                MovieReleaseYear = 2002,
                MovieDirector = "Chris Columbus",
                MoviePicture = "https://www.imdb.com/title/tt0295297/mediaviewer/rm1029675264/",
                MovieTrailer = "https://www.youtube.com/watch?v=jBltxS8HfQ4",
                FranchiseId = 2
            });

            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterId = 1, CharacterFullName = "Jennifer Lawrence", CharacterAlias = "Katniss Everdeen",
                CharacterGender = "Female",
                CharacterPicture =
                    "https://www.imdb.com/title/tt1392170/mediaviewer/rm2148839936/?ft0=name&fv0=nm2225369&ft1=image_type&fv1=still_frame"
            });

            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterId = 2,
                CharacterFullName = "Sean Skinner",
                CharacterAlias = "Harry Potter",
                CharacterGender = "StrongMale",
                CharacterPicture =
                    "https://www.imdb.com/title/tt0241527/mediaviewer/rm785201408/"
            });


            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterId = 3,
                CharacterFullName = "Neve Campbell",
                CharacterAlias = "Kiara",
                CharacterGender = "Female",
                CharacterPicture =
                    "https://lionking.fandom.com/wiki/Kiara?file=Kiara-HD.png"

            });

            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterId = 4, CharacterFullName = "Josh Hutcherson", CharacterAlias = "Peeta Mellark",
                CharacterGender = "Male",
                CharacterPicture =
                    "https://www.imdb.com/title/tt1951265/mediaviewer/rm1779759872/?ft0=name&fv0=nm1242688&ft1=image_type&fv1=still_frame"
            });

            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "CharactersMovie",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 },
                            new { MovieId = 1, CharacterId = 3 },
                            new { MovieId = 2, CharacterId = 2 }

                        );
                    });

        }
    }
}
