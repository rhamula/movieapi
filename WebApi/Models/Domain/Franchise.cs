﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesApi.Models
{
   public  class Franchise
   {
        
        public int FranchiseId { get; set; }
        [Required]
        [MaxLength(50)]
        public string FranchiseName { get; set; }
        [Required]
        [MaxLength(100)]
        public string FranchiseDescription { get; set; }

        public ICollection<Movie> Movies { get; set; }




   } 
}
