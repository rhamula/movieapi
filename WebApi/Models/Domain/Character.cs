﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesApi.Models
{
    public class Character
    {
        // Pk
        public int CharacterId { get; set; }

        // Fields
        [Required]
        [MaxLength(100)]
        public string CharacterFullName { get; set; }
        
        [MaxLength(50)]
        public string CharacterAlias { get; set; }
        [Required]
        [MaxLength(50)]
        public string CharacterGender { get; set; }
        [MaxLength(1000)]
        public string CharacterPicture { get; set; }

        // Relationships
        public ICollection<Movie> Movies { get; set; }
        
        

    }
}
