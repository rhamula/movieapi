﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace MoviesApi.Models
{
    public class Movie
    {

        public int MovieId { get; set; }
        [Required]
        [MaxLength(50)]
        public string MovieTitle { get; set; }
        [MaxLength(200)]
        public string MovieGenre { get; set; }
        [Required]
        [MaxLength(100)]
        public int MovieReleaseYear { get; set; }
        [MaxLength(50)]
        public string MovieDirector { get; set; }
        [MaxLength(1000)]
        public string MoviePicture { get; set; }
        [MaxLength(1000)]
        public string MovieTrailer { get; set; }
        
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<Character> Characters { get; set; }

    }
}
