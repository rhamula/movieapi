﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesApi.Models;
using WebApi.Models.DTOs.Franchise;
using WebApi.Models.DTOs.MovieFranchise;

namespace WebApi.Controller
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly MoviesDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Getting all franchise
        /// </summary>
        /// <returns>Returns a task of action</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetAllFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises.Include(f => f.Movies).ToListAsync());

        }

        /// <summary>
        /// Gets all movies in franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns>Returns a task with movies</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<MovieFranchiseReadDTO>> GetAllMoviesInFranchise(int id)
        {
            Franchise franchise= await _context.Franchises.Include(m => m.Movies).Where(f => f.FranchiseId == id)
                .FirstAsync();

            if (franchise == null)
            {
                return NotFound();
            }
          
            return _mapper.Map<MovieFranchiseReadDTO>(franchise);

        }

        /// <summary>
        /// Gets character in franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns>Returns a task of characters</returns>
        [HttpGet("{id}/character")]
        public async Task<ActionResult<CharacterFranchiseReadDTO>> GetCharactersInFranchise(int id)
        {

            List<Character> characterList = await _context.Movies.Where(m => m.FranchiseId == id).SelectMany(x => x.Characters).Distinct()
                .ToListAsync();

            if (characterList == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterFranchiseReadDTO>(characterList);

        } 


       /// <summary>
       /// Getting specific franchise by id
       /// </summary>
       /// <param name="id">id of franchise</param>
       /// <returns>Returns task of with a franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies)
                .FirstOrDefaultAsync(f => f.FranchiseId == id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

       /// <summary>
       /// Update a franchise
       /// </summary>
       /// <param name="id">franchise id</param>
       /// <param name="updatedFranchise">DTO of franchise</param>
       /// <returns>Returns a task action</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO updatedFranchise)
        {
            if (id != updatedFranchise.FranchiseId)
            {
                return BadRequest();
            }

            //Map to domain
            Franchise domainFranchise = _mapper.Map<Franchise>(updatedFranchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add a new franchise to DB
        /// </summary>
        /// <param name="franchiseDto">DTO of franchise</param>
        /// <returns>Returns a task of action</returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> AddFranchise(FranchiseCreateDTO franchiseDto)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDto);
            _context.Franchises.Add(domainFranchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchiseById", new { id = domainFranchise.FranchiseId }, _mapper.Map<FranchiseCreateDTO>(domainFranchise));
        }

      /// <summary>
      /// Deletes specific franchise by id
      /// </summary>
      /// <param name="id">id of franchise</param>
      /// <returns>Returns task with action</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            Franchise franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            var movies = _context.Movies.Where(m => m.FranchiseId == id);

            foreach (Movie movie in movies)
            {
                movie.FranchiseId = null;
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Checks if franchise exists
        /// </summary>
        /// <param name="id">id of franchise</param>
        /// <returns>Returns a boolean true or false</returns>
        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }
    }
}
