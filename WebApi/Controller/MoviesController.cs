﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesApi.Models;
using WebApi.Models.DTOs.CharacterMovie;
using WebApi.Models.DTOs.Movie;
using WebApi.Models.DTOs.MovieFranchise;

namespace WebApi.Controller
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly MoviesDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all movies
        /// </summary>
        /// <returns>Returns a task of movies</returns>
        [HttpGet] 
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _context.Movies.Include(m=>m.Characters).ToListAsync());
        }

        /// <summary>
        /// Gets a movie by specific id
        /// </summary>
        /// <param name="id">Movie id</param>
        /// <returns>Returns a task with a movie</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Movie>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return movie;
        }
        /// <summary>
        /// Gets all character in a movie
        /// </summary>
        /// <param name="id">Character id</param>
        /// <returns>Returns a task with all characters in the movie</returns>
        [HttpGet("/{id}/Characters")]
        public async Task<ActionResult<CharacterMovieReadDTO>> GetAllCharacterInMovie(int id)
        {
            Movie movieList = await _context.Movies.Include(m => m.Characters).Where(f => f.MovieId == id)
                .FirstAsync();

            if (movieList == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterMovieReadDTO>(movieList);
        }

        /// <summary>
        /// Updates a movie
        /// </summary>
        /// <param name="id">Movie id</param>
        /// <param name="movieDto">MovieDTO</param>
        /// <returns>Returns a task of action</returns>
            [HttpPut("{id}")]
        public async Task<IActionResult> UpdateMovie(int id, MovieEditDTO movieDto)
        {
            if (id != movieDto.MovieId)
            {
                return BadRequest();
            }

            //Map to domain
            Movie domainMovie = _mapper.Map<Movie>(movieDto);
            _context.Entry(domainMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

       /// <summary>
       /// Add a movie to franchise
       /// </summary>
       /// <param name="movieId">Movie id</param>
       /// <param name="franchiseId">Franchise id</param>
       /// <returns>Returns a task of action</returns>
        [HttpPut("{movieId}/{franchiseId}")]
        public async Task<IActionResult> AddMovieToFranchise(int movieId, int franchiseId)
        {
            Movie movie = await _context.Movies.FindAsync(movieId);
            if (movie == null)
            {
                return NotFound();
            }

            movie.FranchiseId = franchiseId;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Removes a movie from existing franchise
        /// </summary>
        /// <param name="id">Movie id</param>
        /// <returns>Returns a task of action</returns>
        [HttpPut("{id}/movie")]
        public async Task<IActionResult> RemoveMovieFromFranchise(int id)
        {
            Movie movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            movie.FranchiseId = null;

            await _context.SaveChangesAsync();

            return NoContent();
        }

       /// <summary>
       /// Adding a movie to DB
       /// </summary>
       /// <param name="movieDto">MovieDTO</param>
       /// <returns>Returns task with a movie</returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> AddMovie(MovieCreateDTO movieDto)
        {
            Movie domainMovie = _mapper.Map<Movie>(movieDto);
            _context.Movies.Add(domainMovie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = domainMovie.MovieId }, _mapper.Map<MovieCreateDTO>(movieDto));
        }

        /// <summary>
        /// Deleting a movie
        /// </summary>
        /// <param name="id">Movie id</param>
        /// <returns>Returns a task of action</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Checks if movie exists
        /// </summary>
        /// <param name="id">Movie id</param>
        /// <returns>Returns a boolean true or false</returns>
        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }
    }
}
