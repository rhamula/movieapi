﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MoviesApi.Models;
using WebApi.Models.DTOs.Character;
using WebApi.Models.DTOs.CharacterMovie;

namespace WebApi.Controller
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly MoviesDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }

       /// <summary>
       /// Gets all characters 
       /// </summary>
       /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _context.Characters.Include(c => c.Movies).ToListAsync());

        }

     /// <summary>
     /// Gets specific character by id
     /// </summary>
     /// <param name="id">id of character</param>
     /// <returns>Returns task with specific character</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _context.Characters.Include(c => c.Movies)
                .FirstOrDefaultAsync(c => c.CharacterId == id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Update a character
        /// </summary>
        /// <param name="id">id of character</param>
        /// <param name="updatedCharacter">characterDTO </param>
        /// <returns>Returns task with an action </returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCharacter(int id, CharacterEditDTO updatedCharacter)
        {
            if (id != updatedCharacter.CharacterId)
            {
                return BadRequest();
            }

            //Map to domain
            Character domainCharacter = _mapper.Map<Character>(updatedCharacter);
            _context.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adding a character to a existing movie
        /// </summary>
        /// <param name="characterId">character id</param>
        /// <param name="movieId">movie id</param>
        /// <param name="characterMovieIds">DTO for character and movies</param>
        /// <returns>Returns a task of action</returns>
        [HttpPut("{characterId}/{movieId}")]
        public async Task<ActionResult> AddCharacterToMovie(int characterId, int movieId, CharacterMovieCreateDTO characterMovieIds)
        {
            if (!CharacterExists(characterId))
            {
                return NotFound();
            }

            if (characterId != characterMovieIds.CharacterId || movieId != characterMovieIds.MovieId)
            {
                return BadRequest();
            }
            Character character = _context.Characters.Include("Movies").First(c => c.CharacterId == characterMovieIds.CharacterId);
            character.Movies.Add(_context.Movies.Find(characterMovieIds.MovieId));
            try
            {
                await _context.SaveChangesAsync();
            }

            catch 
            {
                throw;
            }

            return CreatedAtAction("AddCharacterToMovie", characterMovieIds);
        }

      /// <summary>
      /// Adding a character to DB
      /// </summary>
      /// <param name="characterDto">CharacterDTO</param>
      /// <returns>Return a task of action</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> AddCharacter(CharacterCreateDTO characterDto)
        {
            Character domainCharacter = _mapper.Map<Character>(characterDto);
            _context.Characters.Add(domainCharacter);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = domainCharacter.CharacterId }, _mapper.Map<CharacterCreateDTO>(domainCharacter));
        }

       /// <summary>
       /// Deletes a character
       /// </summary>
       /// <param name="id">id of character</param>
       /// <returns>Return a task of action</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

       /// <summary>
       /// Removes a character from existing movie
       /// </summary>
       /// <param name="characterId">Character id</param>
       /// <param name="movieId">Movie id</param>
       /// <param name="characterMovieDeleteDto">CharacterDTO</param>
       /// <returns>Returns a task with action</returns>
        [HttpDelete("{characterId}/{movieId}")]
        public async Task<IActionResult> RemoveCharacterFromExistingMovie(int characterId, int movieId,CharacterMovieDeleteDTO characterMovieDeleteDto)
        {

            Character character = _context.Characters.Include("Movies").First(c => c.CharacterId == characterMovieDeleteDto.CharacterId);

            if (character == null)
            {
                return NotFound();
            }

            character.Movies.Remove(_context.Movies.Find(characterMovieDeleteDto.MovieId));

            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                throw;
            }

            return NoContent();

        }

        /// <summary>
        /// Check if character exists
        /// </summary>
        /// <param name="id">id of character</param>
        /// <returns>Return boolean true or false</returns>
        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(c => c.CharacterId == id);
        }

       
        

    }
}
